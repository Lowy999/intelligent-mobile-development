// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})

const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  const openId = cloud.getWXContext().OPENID
  db.collection('product').where({
    _id: event.id
  }).update({
    data: {
      title: event.title,
      price: event.price,
      storage: event.storage,
      image: event.image,
      detail: event.detail
    }
  })
}
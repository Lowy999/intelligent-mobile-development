// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})

const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  const openId = cloud.getWXContext().OPENID
  db.collection('product').add({
    data: {
      openId: openId,
      title: event.title,
      price: event.price,
      sales: 0,
      storage: event.storage,
      image: event.image,
      detail: event.detail
    }
  })
}
// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})

const db = cloud.database()
const _ = db.command
exports.main = async (event, context) => {
  return db.collection('product').where({
    title:{
      $regex:'.*' + event.title + '.*',
      $options: 'i'
    },
    price: _.gte(event.minPrice).and(_.lte(event.maxPrice))
  }).get()
}
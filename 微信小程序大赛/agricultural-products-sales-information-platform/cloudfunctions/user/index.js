// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})

const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  const openId = cloud.getWXContext().OPENID
  switch (event.function) {
    case 'addUserInfo':
      db.collection('user').add({
        data: {
          openId: openId,
          phone: event.phone,
          address: event.address
        }
      })
      break
    case 'updateUserInfo':
      db.collection('user').where({
        openId: openId
      }).update({
        data: {
          phone: event.phone,
          address: event.address
        }
      })
      break
    case 'getUserInfo':
      return db.collection('user').where({
        openId: openId
      }).get()
    default:
  }
}
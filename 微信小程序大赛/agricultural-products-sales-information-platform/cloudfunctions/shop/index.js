// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})

const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  const openId = cloud.getWXContext().OPENID
  if (event.function === 'createShop') {
    db.collection('shop').add({
      data: {
        openId: openId
      }
    })
  } else if (event.function === 'getShop') {
    return db.collection('shop').where({
      openId: openId
    }).get()
  } else if (event.function === 'getProductsInShop') {
    return db.collection('product').where({
      openId: openId
    }).get()
  }
}
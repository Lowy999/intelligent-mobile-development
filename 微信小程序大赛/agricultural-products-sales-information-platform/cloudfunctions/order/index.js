// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})

const db = cloud.database()
const _ = db.command
// 云函数入口函数
exports.main = async (event, context) => {
  const openId = cloud.getWXContext().OPENID
  if (event.function === 'addOrder') {
    db.collection('order').add({
      data: {
        openId: openId,
        productId: event.productId,
        status: '未完成',
        createTime: db.serverDate()
      }
    })
  } else if (event.function === 'getUserOrder') {
    return db.collection('order')
      .aggregate()
      .match({
        openId: openId
      })
      .lookup({
        from: 'product',
        localField: 'productId',
        foreignField: '_id',
        as: 'product'
      })
      .lookup({
        from: 'user',
        localField: 'openId',
        foreignField: 'openId',
        as: 'user'
      })
      .sort({
        createTime: -1
      })
      .end()
  } else if (event.function === 'getShopOrder') {
    return db.collection('order')
      .aggregate()
      .lookup({
        from: 'product',
        localField: 'productId',
        foreignField: '_id',
        as: 'product'
      })
      .match({
        product: _.elemMatch({
          openId: openId,
        })
      })
      .lookup({
        from: 'user',
        localField: 'openId',
        foreignField: 'openId',
        as: 'user'
      })
      .sort({
        createTime: -1
      })
      .end()
  } else if (event.function === 'finishOrder') {
    db.collection('order').doc(event.id).update({
      data: {
        status: '已完成'
      }
    })
  } else if (event.function === 'commentOrder') {
    db.collection('order').doc(event.id).update({
      data: {
        comment: event.comment
      }
    })
  } else if (event.function === 'getComment') {
    return db.collection('order').where({
      productId: event.productId,
      comment: _.exists(true)
    }).get()
  }
}
// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})

const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  const openId = cloud.getWXContext().OPENID
  if (event.function === 'getCourseList') {
    return db.collection('help_farmer_course').get()
  } else if (event.function === 'getCourse') {
    return db.collection('help_farmer_course').doc(event.id).get()
  } 
}
// pages/helpFarmerCourse/index.js
Page({
  data: {
    courseList: []
  },
  getCourseList() {
    var that = this
    wx.cloud.callFunction({
      name: 'helpFarmerCourse',
      data: {
        function: 'getCourseList'
      },
      success(res) {
        that.setData({
          courseList: res.result.data
        })
      }
    })
  },
  toDetailPage(e) {
    wx.navigateTo({
      url: '/pages/helpFarmerCourse/detail?courseId=' + e.currentTarget.dataset.courseid,
    })
  },
  onLoad(options) {
    this.getCourseList()
  },
})
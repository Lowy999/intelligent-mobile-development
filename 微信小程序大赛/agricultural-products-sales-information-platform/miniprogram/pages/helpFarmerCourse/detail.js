// pages/helpFarmerCourse/detail.js
Page({
  data: {
    course: {}
  },
  onLoad(options) {
    var that = this
    wx.cloud.callFunction({
      name: 'helpFarmerCourse',
      data: {
        function: 'getCourse',
        id: options.courseId
      },
      success(res) {
        that.setData({
          course: res.result.data
        })
      }
    })
  },
})
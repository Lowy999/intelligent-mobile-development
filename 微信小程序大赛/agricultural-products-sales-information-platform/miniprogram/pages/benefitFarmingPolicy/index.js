// pages/benefitFarmingPolicy/index.js
Page({
  data: {
    policyList: []
  },
  getPolicyList() {
    var that = this
    wx.cloud.callFunction({
      name: 'benefitFarmingPolicy',
      data: {
        function: 'getPolicyList'
      },
      success(res) {
        that.setData({
          policyList: res.result.data
        })
      }
    })
  },
  toDetailPage(e) {
    wx.navigateTo({
      url: '/pages/benefitFarmingPolicy/detail?policyId=' + e.currentTarget.dataset.policyid,
    })
  },
  onLoad(options) {
    this.getPolicyList()
  },
})
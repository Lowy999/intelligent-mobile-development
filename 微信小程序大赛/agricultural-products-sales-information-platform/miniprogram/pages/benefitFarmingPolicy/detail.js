// pages/benefitFarmingPolicy/detail.js
Page({
  data: {
    policy: {}
  },
  onLoad(options) {
    var that = this
    wx.cloud.callFunction({
      name: 'benefitFarmingPolicy',
      data: {
        function: 'getPolicy',
        id: options.policyId
      },
      success(res) {
        that.setData({
          policy: res.result.data
        })
      }
    })
  },
})
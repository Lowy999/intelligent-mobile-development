Page({
  data: {
    products: [],
    showModalStatus: false,
    searchContent: '',
    minPrice: 0,
    maxPrice: 1000000
  },
  onShow(options) {
    this.searchProduct()
  },
  toDetailPage(e) {
    wx.navigateTo({
      url: '/pages/display/detail?productId=' + e.currentTarget.dataset.productid,
    })
  },
  searchContentInput(e) {
    this.setData({
      searchContent: e.detail.value
    })
  },
  searchProduct() {
    var that = this
    wx.cloud.callFunction({
      name: "searchProduct",
      data: {
        title: that.data.searchContent,
        minPrice: that.data.minPrice,
        maxPrice: that.data.maxPrice
      },
      success(res) {
        that.setData({
          products: res.result.data
        })
      }
    })
  },
  powerDrawer: function (e) {
    var currentStatu = e.currentTarget.dataset.statu;
    this.util(currentStatu)
  },
  util: function (currentStatu) {
    var animation = wx.createAnimation({
      duration: 200,
      timingFunction: "linear",
      delay: 0
    });
    this.animation = animation;
    animation.translateY(240).step();
    this.setData({
      animationData: animation.export()
    })
    setTimeout(function () {
      animation.translateY(0).step()
      this.setData({
        animationData: animation
      })
      if (currentStatu == "close") {
        this.setData(
          {
            showModalStatus: false
          }
        );
      }
    }.bind(this), 200)
    if (currentStatu == "open") {
      this.setData(
        {
          showModalStatus: true
        }
      );
    }
  },
  formSubmit(e) {
    this.setData({
      minPrice: Number(e.detail.value.minPrice),
      maxPrice: Number(e.detail.value.maxPrice)
    })
    this.searchProduct()
    this.util("close")
  }
})
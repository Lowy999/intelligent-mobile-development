// pages/display/detail.js
Page({
  data: {
    product: null,
    comment: null
  },
  onLoad(options) {
    this.getProduct(options.productId)
    this.getComment(options.productId)
  },
  getComment(id) {
    var that = this
    wx.cloud.callFunction({
      name: 'order',
      data: {
        function: 'getComment',
        productId: id
      },
      success: function (res) {
        that.setData({
          comment: res.result.data
        })
      }
    })
  },
  getProduct(id) {
    var that = this
    wx.cloud.callFunction({
      name: 'getProduct',
      data: {
        id: id
      },
      success: function (res) {
        that.setData({
          product: res.result.data[0]
        })
      }
    })
  },
  previewImage: function (e) {
    var current = e.target.dataset.src;
    wx.previewImage({
      current: current,
      urls: [current]
    })
  },
  immeBuy() {
    var that = this
    wx.cloud.callFunction({
      name: 'order',
      data: {
        function: 'addOrder',
        productId: that.data.product._id
      },
      success() {
        wx.showToast({
          title: '成功',
          icon: 'success',
          duration: 2000
        })
      }
    })
  }
})
// pages/store/index.js
Page({
  data: {
    hasShop: false,
    hasLogin: false,
    products: [],
    showModalStatus: false,
    image: null,
    selectedProduct: {},
    hasSelected: false
  },
  onShow(options) {
    this.getShop()
    this.getUserInfo()
    this.getProductsInShop()
  },
  createShop() {
    if (this.data.hasLogin) {
      wx.cloud.callFunction({
        name: 'shop',
        data: {
          function: 'createShop'
        }
      })
    } else {
      wx.switchTab({
        url: '/pages/user/index',
      })
    }
  },
  getShop() {
    var that = this
    wx.cloud.callFunction({
      name: 'shop',
      data: {
        function: 'getShop'
      },
      success: function () {
        that.setData({
          hasShop: true
        })
      }
    })
  },
  getUserInfo() {
    var that = this
    wx.getStorage({
      key: "userInfo",
      success(res) {
        that.setData({
          hasLogin: true
        })
      }
    })
  },
  powerDrawer: function (e) {
    var currentStatu = e.currentTarget.dataset.statu;
    this.setData({
      image: null,
      hasSelected: false
    })
    this.util(currentStatu)
  },
  util: function (currentStatu) {
    var animation = wx.createAnimation({
      duration: 200,
      timingFunction: "linear",
      delay: 0
    });
    this.animation = animation;
    animation.translateY(240).step();
    this.setData({
      animationData: animation.export()
    })
    setTimeout(function () {
      animation.translateY(0).step()
      this.setData({
        animationData: animation
      })
      if (currentStatu == "close") {
        this.setData(
          {
            showModalStatus: false
          }
        );
      }
    }.bind(this), 200)
    if (currentStatu == "open") {
      this.setData(
        {
          showModalStatus: true
        }
      );
    }
  },
  formSubmit(e) {
    var that = this
    if (this.data.image.slice(0, 5) === "cloud") {
      wx.cloud.callFunction({
        name: 'upgradeProduct',
        data: {
          title: e.detail.value.title,
          price: Number(e.detail.value.price),
          storage: Number(e.detail.value.storage),
          detail: e.detail.value.detail,
          image: that.data.selectedProduct.image,
          id: that.data.selectedProduct._id
        },
        success: function () {
          that.onShow()
          that.util("close")
        }
      })
    } else {
      wx.cloud.uploadFile({
        cloudPath: that.data.image.split("/")[that.data.image.split("/").length - 1],
        filePath: that.data.image,
        success: res => {
          if (!that.data.hasSelected) {
            wx.cloud.callFunction({
              name: 'createProduct',
              data: {
                title: e.detail.value.title,
                price: Number(e.detail.value.price),
                storage: Number(e.detail.value.storage),
                detail: e.detail.value.detail,
                image: res.fileID
              },
              success: function () {
                that.onShow()
                that.util("close")
              }
            })
          } else {
            wx.cloud.callFunction({
              name: 'upgradeProduct',
              data: {
                title: e.detail.value.title,
                price: Number(e.detail.value.price),
                storage: Number(e.detail.value.storage),
                detail: e.detail.value.detail,
                image: res.fileID,
                id: that.data.selectedProduct._id
              },
              success: function () {
                that.onShow()
                that.util("close")
              }
            })
          }
        }
      })
    }
  },
  upload() {
    wx.chooseImage({
      success: chooseResult => {
        this.setData({
          image: chooseResult.tempFilePaths[0]
        })
      }
    })

  },
  getProductsInShop() {
    var that = this
    wx.cloud.callFunction({
      name: 'shop',
      data: {
        function: 'getProductsInShop'
      },
      success: function (res) {
        that.setData({
          products: res.result.data
        })
      }
    })
  },
  deleteProduct(e) {
    var that = this
    wx.cloud.callFunction({
      name: 'deleteProduct',
      data: {
        id: e.currentTarget.dataset.productid
      },
      success() {
        that.onShow()
      }
    })
  },
  upgradeProduct(e) {
    var that = this
    wx.cloud.callFunction({
      name: 'getProduct',
      data: {
        id: e.currentTarget.dataset.productid
      },
      success(res) {
        that.setData({
          selectedProduct: res.result.data[0],
          image: res.result.data[0].image,
          hasSelected: true
        })
        that.util("open")
      }
    })
  }
})
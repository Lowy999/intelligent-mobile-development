// pages/user/index.js
Page({
  data: {
    userInfo: null,
    hasLogin: false,
    showModalStatus: false,
    address: '',
    phone: '',
    hasReceiveInfo: false
  },
  exit() {
    var that = this
    wx.removeStorage({
      key: 'userInfo',
      success() {
        that.onLoad()
      }
    })
  },
  getUserInfo() {
    var that = this
    wx.getStorage({
      key: "userInfo",
      success(res) {
        that.setData({
          userInfo: res.data,
          hasLogin: true
        })
      },
      fail() {
        that.setData({
          userInfo: {
            nickName: "请登录",
            avatarUrl: "../../images/user.png"
          },
          hasLogin: false
        })
      }
    })
  },
  getUserProfile(e) {
    var that = this
    wx.getUserProfile({
      desc: '用于完善会员资料',
      success: (res) => {
        wx.setStorage({
          key: "userInfo",
          data: res.userInfo
        })
        that.onLoad()
      }
    })
  },
  powerDrawer: function (e) {
    var currentStatu = e.currentTarget.dataset.statu;
    this.util(currentStatu)
  },
  util: function (currentStatu) {
    var animation = wx.createAnimation({
      duration: 200,
      timingFunction: "linear",
      delay: 0
    });
    this.animation = animation;
    animation.translateY(240).step();
    this.setData({
      animationData: animation.export()
    })
    setTimeout(function () {
      animation.translateY(0).step()
      this.setData({
        animationData: animation
      })
      if (currentStatu == "close") {
        this.setData(
          {
            showModalStatus: false
          }
        );
      }
    }.bind(this), 200)
    if (currentStatu == "open") {
      this.setData(
        {
          showModalStatus: true
        }
      );
    }
  },
  toOrderUser() {
    wx.navigateTo({
      url: '/pages/order/user',
    })
  },
  toOrderShop() {
    wx.navigateTo({
      url: '/pages/order/shop',
    })
  },
  formSubmit(e) {
    var that = this
    if (this.data.hasReceiveInfo) {
      wx.cloud.callFunction({
        name: 'user',
        data: {
          function: 'updateUserInfo',
          phone: e.detail.value.phone,
          address: e.detail.value.address
        },
        success(res) {
          that.util('close')
          that.getUserReceiveInfo()
          console.log(res)
        }
      })
    } else {
      wx.cloud.callFunction({
        name: 'user',
        data: {
          function: 'addUserInfo',
          phone: e.detail.value.phone,
          address: e.detail.value.address
        },
        success() {
          that.util('close')
          that.getUserReceiveInfo()
        }
      })
    }
  },
  getUserReceiveInfo() {
    var that = this
    wx.cloud.callFunction({
      name: 'user',
      data: {
        function: 'getUserInfo'
      },
      success(e) {
        that.setData({
          address: e.result.data[0].address,
          phone: e.result.data[0].phone,
          hasReceiveInfo: true
        })
      }
    })
  },
  toBenefitFarmingPolicy() {
    wx.navigateTo({
      url: '/pages/benefitFarmingPolicy/index',
    })
  },
  toHelpFarmerCourse() {
    wx.navigateTo({
      url: '/pages/helpFarmerCourse/index',
    })
  },
  onLoad() {
    this.getUserInfo()
    this.getUserReceiveInfo()
  }
})
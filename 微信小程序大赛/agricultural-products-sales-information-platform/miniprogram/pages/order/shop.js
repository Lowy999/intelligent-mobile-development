// pages/order/shop.js
Page({
  data: {
    order: []
  },
  getOrder() {
    var that = this
    wx.cloud.callFunction({
      name: 'order',
      data: {
        function: 'getShopOrder'
      },
      success(res) {
        console.log(res)
        that.setData({
          order: res.result.list
        })
      }
    })
  },
  finishOrder(e) {
    var that = this
    wx.cloud.callFunction({
      name: 'order',
      data: {
        function: 'finishOrder',
        id: e.currentTarget.dataset.id
      },
      success() {
        that.getOrder()
      }
    })
  },
  onLoad(options) {
    this.getOrder()
  },
})
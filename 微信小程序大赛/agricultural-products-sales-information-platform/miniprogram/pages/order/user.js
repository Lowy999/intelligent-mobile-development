// pages/order/user.js
Page({
  data: {
    order: [],
    selectedOrderId: null
  },
  getOrder() {
    var that = this
    wx.cloud.callFunction({
      name: 'order',
      data: {
        function: 'getUserOrder'
      },
      success(res) {
        that.setData({
          order: res.result.list.map((item)=>{
            item.createTime = item.createTime.substring(0, 10) 
            return item
          })
        })
      }
    })
  },
  powerDrawer: function (e) {
    var currentStatu = e.currentTarget.dataset.statu;
    this.setData({
      image: null,
      hasSelected: false
    })
    this.util(currentStatu)
  },
  util: function (currentStatu) {
    var animation = wx.createAnimation({
      duration: 200,
      timingFunction: "linear",
      delay: 0
    });
    this.animation = animation;
    animation.translateY(240).step();
    this.setData({
      animationData: animation.export()
    })
    setTimeout(function () {
      animation.translateY(0).step()
      this.setData({
        animationData: animation
      })
      if (currentStatu == "close") {
        this.setData(
          {
            showModalStatus: false
          }
        );
      }
    }.bind(this), 200)
    if (currentStatu == "open") {
      this.setData(
        {
          showModalStatus: true
        }
      );
    }
  },
  commentOrder(e) {
    this.setData({
      selectedOrderId: e.currentTarget.dataset.orderid
    })
    this.util('open')
  },
  formSubmit(e) {
    var that = this
    wx.cloud.callFunction({
      name: 'order',
      data: {
        function: 'commentOrder',
        id: that.data.selectedOrderId,
        comment: e.detail.value.comment
      },
      success() {
        that.util('close')
      }
    })
  },
  onLoad(options) {
    this.getOrder()
  },
})